import React, { Component } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import MyCard from './components/MyCard'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavMenu from './components/NavMenu';
import TableData from './components/TableData';

export default class App extends Component {
  
  render() {
    return (
      <Container >
        <NavMenu/>
        <Row >
          <Col xs={6} md={4}>
            <MyCard />
          </Col>
          <Col xs={12} md={8} >
            <TableData/>
          </Col>
        </Row>
      </Container>
    )
  }
}

