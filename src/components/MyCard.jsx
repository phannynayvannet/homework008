import React, { Component } from 'react'
import { Form, Button, Image } from 'react-bootstrap'

export default class MyCard extends Component {

  constructor(props) {
    super(props);
    this.state = ({
      username: "",
      email: "",
      password: "",
      emailErr: "",
      passwordErr: "",
      signIn: false,
    })
  }

  onHandleText = (e) => {
    this.setState({
      [e.target.name]: e.target.value
      //email: e.target.value
    }, () => {
      console.log("Email:", this.state);

      if (e.target.name === "email") {
        let pattern = /^\S+@\S+\.[a-z]{3}$/g;
        let result = pattern.test(this.state.email.trim())

        if (result) {
          this.setState({
            emailErr: ""
          })
        } else if (this.state.email === "") {
          this.setState({
            emailErr: "can't empty!"
          })
        } else {
          this.setState({
            emailErr: "invalid email!"
          })
        }
      }

      if (e.target.name === "password") {
        let pattern = /^[a-zA-Z0-9_\W]{8,}$/g;
        let result = pattern.test(this.state.password.trim())

        if (result) {
          this.setState({
            passwordErr: ""
          })
        } else if (this.state.password === "") {
          this.setState({
            passwordErr: "can't empty!"
          })
        } else {
          this.setState({
            passwordErr: "invalid password!(must have 8 charaters)"
          })
        }
      }
    })
  }

  signInBtn = () => {
    if (this.state.email !== "" && this.state.password !== "" && this.state.emailErr === "" && this.state.passwordErr === "") {
      return false
    } else {
      return true
    }
  }
  render() {
    return (
      <Form style={{ marginTop: '50px' }}>
        <div style={{ display: 'block', textAlign: 'center' }}>
          <svg xmlns="http://www.w3.org/2000/svg" width="160" height="160" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
            <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
          </svg>
          {/* <Image style={{display:'flex',justifyContent:'center',alignContent:'center'}} width="200" height="200" src="https://www.nicepng.com/png/detail/501-5010656_my-account-comments-my-account-icon-vector.png" roundedCircle /> */}
        </div>
        <h1 style={{ display: 'block', textAlign: 'center' }}>
          User Account
          </h1>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Username</Form.Label>
          <Form.Control name="username" onChange={this.onHandleText} type="username" placeholder="Username" />
          <Form.Text className="text-muted">
            {/* <p style={{ color: "red" }}>{this.state.emailErr}</p> */}
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
          <Form.Label>Gender</Form.Label>
          <Form>
            {['radio'].map((type) => (
              <div key={`custom-inline-${type}`} className="mb-3">
                <Form.Check
                  custom
                  name="sex"
                  inline
                  label="Male"
                  type={type}
                  id={`custom-inline-${type}-1`}
                />
                <Form.Check
                  custom
                  name="sex"
                  inline
                  label="Female"
                  type={type}
                  id={`custom-inline-${type}-2`}
                />
              </div>
            ))}
          </Form>
        </Form.Group>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control name="email" onChange={this.onHandleText} type="email" placeholder="Email" />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.emailErr}</p>
          </Form.Text>
        </Form.Group>


        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control name="password" onChange={this.onHandleText} type="password" placeholder="Password" />
          <Form.Text className="text-muted">
            <p style={{ color: "red" }}>{this.state.passwordErr}</p>
          </Form.Text>
        </Form.Group>
        <Button disabled={this.signInBtn()} variant="primary" type="button">
          Save
        </Button>
      </Form>
    )
  }
}
