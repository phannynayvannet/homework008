import React from 'react'
import { Container, Row, Table, Button, Badge } from 'react-bootstrap'

function TableData({ items }) {


    return (
        <Container style={{ marginTop: '250px' }}>
            <h2>Table Account</h2>
            <Table striped bordered hover >
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Gender</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </Table>

            <Button
                variant="danger"
                className="mx-2"
            >Delete</Button>

        </Container>
    )
}
export default TableData
